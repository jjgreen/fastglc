fastglc documentation
--------------------

The docbook source in `fastglc.xml` is used to generate the Unix
man-page `fastglc.3` and the plain text `fastglc.txt`. The tools
`xsltproc` and `lynx` are required to regenerate these files.

The script `fastglc-fetch.sh` pulls the latest version of `fastglc.c`
and `fastglc.h` from the GitHub master to the current directory.
